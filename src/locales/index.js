import cartItem from './cartItem'
import cart from './cart'

export default {
  ru: {
    priceWithCurrency: '{price} руб.',
    cartItem,
    cart,
  },
}
