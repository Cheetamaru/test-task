const labels = {
  itemTitleAndDescription: 'Наименование товара и описание',
  quantity: 'Количество',
  price: 'Цена',
  cartIsEmpty: 'Корзина пуста',
  totalSum: 'Общая стоимость:',
}

export default {
  labels,
}
