const labels = {
  itemsShortening: 'шт.',
  lowStock: 'Количество ограничено',
  deleteItem: 'Удалить',
}

export default {
  labels,
}
