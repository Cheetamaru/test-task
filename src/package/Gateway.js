import itemsData from '@/data/data.json'
import namesData from '@/data/names.json'
import { randomNumberFromInterval } from '@/utils'

class Gateway {
  getItemsResponse () {
    return itemsData
  }

  getItemNames () {
    return namesData
  }

  getDollarExchangeRate () {
    return Number(randomNumberFromInterval(20, 80).toFixed(2))
  }
}

export default new Gateway()
