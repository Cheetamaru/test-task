import { usdToRub } from '@/utils'

export class ItemsParser {
  constructor ({itemsResponse, names, exchangeRate}) {
    this.formattedItems = this._formatItems(itemsResponse, exchangeRate)
    this.formattedNames = this._formatNames(names)
  }

  getParsedItems () {
    return this._getParsedItems()
  }

  getParsedFilteredItems () {
    const parsedItems = this._getParsedItems()
    return parsedItems
      .map((group) => {
        const modifiedGroup = { ...group }
        modifiedGroup.items = modifiedGroup.items.filter(item => item.stock)
        return modifiedGroup
      })
      .filter(group => group.items.length)
  }

  _formatItems (itemsResponse, exchangeRate) {
    const formatItems = (elem) => {
      return {
        dollarPrice: elem['C'],
        price: usdToRub(elem['C'], exchangeRate),
        groupId: String(elem['G']),
        itemId: String(elem['T']),
        stock: elem['P'],
      }
    }

    return itemsResponse['Value']['Goods'].map(formatItems)
  }

  _formatNames (names) {
    const namesEntries = Object.entries(names)

    const formatItemsNames = (itemEntry) => {
      return {
        itemId: itemEntry[0],
        itemTitle: itemEntry[1]['N'],
      }
    }

    const formatGroups = (entry) => {
      return {
        groupId: entry[0],
        groupName: entry[1]['G'],
        items: Object.entries(entry[1]['B']).map(formatItemsNames),
      }
    }

    return namesEntries.map(formatGroups)
  }

  _getParsedItems () {
    const formattedItems = this.formattedItems
    const formattedNames = this.formattedNames

    const parseItemGroups = (elem) => {
      return {
        groupId: elem.groupId,
        groupTitle: elem.groupName,
        items: elem.items.map((item) => {
          const itemInfo = formattedItems.find((el) => {
            return el.groupId === elem.groupId && el.itemId === item.itemId
          })
          return {
            itemId: item.itemId,
            itemTitle: item.itemTitle,
            stock: itemInfo ? itemInfo.stock : 0,
            price: itemInfo ? itemInfo.price : 0,
            dollarPrice: itemInfo ? itemInfo.dollarPrice : 0,
          }
        }),
      }
    }

    return formattedNames.map(parseItemGroups)
  }
}
