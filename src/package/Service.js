import { ItemsParser } from '@/package/ItemsParcer'

class Service {
  constructor (gateway, store) {
    this.$gateway = gateway
    this.$store = store
  }

  getItemsObject () {
    const itemsResponse = this.$gateway.getItemsResponse()
    const names = this.$gateway.getItemNames()
    const exchangeRate = this.$gateway.getDollarExchangeRate()

    this.$store.dispatch('setExchangeRate', exchangeRate)

    return this._parseItems(itemsResponse, names, exchangeRate)
  }

  _parseItems (itemsResponse, names, exchangeRate) {
    const itemsParser = new ItemsParser({ itemsResponse, names, exchangeRate })
    return itemsParser.getParsedFilteredItems()
  }
}

export default Service
