import Vue from 'vue'
import VueI18n from 'vue-i18n'
import messages from '@/locales'
import App from './App.vue'
import store from './store'
import Service from '@/package/Service'
import gateway from '@/package/Gateway'

Vue.config.productionTip = false
Vue.prototype.$service = new Service(gateway, store)

Vue.use(VueI18n)
const i18n = new VueI18n({
  locale: 'ru',
  messages,
})

new Vue({
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app')
