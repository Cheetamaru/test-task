export function usdToRub (usdPrice, exchangeRate) {
  return Number((usdPrice * exchangeRate).toFixed(2))
}


export function randomNumberFromInterval (min, max) {
  return Math.random() * (max - min + 1) + min
}
