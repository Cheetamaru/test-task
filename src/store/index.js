import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    exchangeRate: 1,
  },
  getters: {
    exchangeRate (state) {
      return state.exchangeRate
    },
  },
  mutations: {
    SET_EXCHANGE_RATE (state, rate) {
      state.exchangeRate = rate
    },
  },
  actions: {
    setExchangeRate ({ commit }, rate) {
      commit('SET_EXCHANGE_RATE', rate)
    },
  },
})
